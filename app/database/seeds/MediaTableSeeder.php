<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class MediaTableSeeder extends Seeder {

	public function run()
	{
		$media = [
			[
				'id' => '100',
				'name' => 'visual arts'
			],
			[
				'id' => '200',
				'name' => 'literary arts'
			],
			[
				'id' => '300',
				'name' => 'performing arts'
			],
			[
				'id' => '400',
				'name' => 'multimedia arts'
			],
			[
				'id' => '500',
				'name' => 'culinary arts'
			]
		];
		$media2 = [
			[
				'id' => '101',
				'name' => 'drawing',
				'parent' => '100'
			],
			[
				'id' => '102',
				'name' => 'painting',
				'parent' => '100'
			],
			[
				'id' => '103',
				'name' => 'ceramics',
				'parent' => '100'
			],
			[
				'id' => '104',
				'name' => 'photography',
				'parent' => '100'
			],
			[
				'id' => '105',
				'name' => 'architecture',
				'parent' => '100'
			],
			[
				'id' => '106',
				'name' => 'sculpture',
				'parent' => '100'
			],
			[
				'id' => '107',
				'name' => 'conceptual art',
				'parent' => '100'
			],
			[
				'id' => '201',
				'name' => 'writing',
				'parent' => '200'
			],
			[
				'id' => '401',
				'name' => 'video games',
				'parent' => '400'
			],
			[
				'id' => '402',
				'name' => 'software design',
				'parent' => '400'
			],
			[
				'id' => '403',
				'name' => 'video',
				'parent' => '400'
			],
			[
				'id' => '404',
				'name' => 'new media',
				'parent' => '400'
			],
			[
				'id' => '301',
				'name' => 'music',
				'parent' => '300'
			],
			[
				'id' => '302',
				'name' => 'dance',
				'parent' => '300'
			],
			[
				'id' => '303',
				'name' => 'theatre',
				'parent' => '300'
			],
			[
				'id' => '304',
				'name' => 'film/cinema',
				'parent' => '300'
			],
			[
				'id' => '501',
				'name' => 'cuisine',
				'parent' => '500'
			],
			[
				'id' => '502',
				'name' => 'confections',
				'parent' => '500'
			]
		];

		foreach($media as $medium)
		{
			Medium::create([
				'id' => $medium['id'],
				'name' => $medium['name']
			]);
		}
		foreach($media2 as $medium)
		{
			Medium::create([
				'id' => $medium['id'],
				'name' => $medium['name'],
				'parent' => $medium['parent']
			]);
		}


	}

}
