<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddContactPreferencesAndArtistIdToMembers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('members', function(Blueprint $table)
		{

			$table->integer('primary_artist_listing')->unsigned()->nullable();
			$table->boolean('contact_mailchimp_tcaa')->nullable()->default(1);
			$table->boolean('contact_calls_to_artists')->nullable()->default(1);
			$table->boolean('contact_class_invites')->nullable()->default(1);
			$table->boolean('contact_list_phone')->nullable()->default(0);
			$table->boolean('contact_contact_form')->nullable()->default(1);

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('members', function(Blueprint $table)
		{
			$table->dropColumn('primary_artist_listing');
			$table->dropColumn('contact_mailchimp_tcaa');
			$table->dropColumn('contact_calls_to_artists');
			$table->dropColumn('contact_class_invites');
			$table->dropColumn('contact_list_phone');
			$table->dropColumn('contact_contact_form');
		});
	}

}
