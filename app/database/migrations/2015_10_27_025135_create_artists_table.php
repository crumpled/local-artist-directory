<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateArtistsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('artists', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('business_name')->nullable();
			$table->string('phone', 20)->nullable();
			$table->date('listing_expiration')->nullable();
			$table->string('photo_url')->nullable();
			$table->boolean('desires_arts_reach_inclusion')->default(false);
			$table->boolean('is_artsreach_approved')->default(false);
			$table->boolean('gives_lessons')->default(false);
			$table->boolean('does_commissions')->default(false);
			$table->string('web_site')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('artists');
	}

}
