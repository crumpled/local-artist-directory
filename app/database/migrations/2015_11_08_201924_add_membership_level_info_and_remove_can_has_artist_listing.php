<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMembershipLevelInfoAndRemoveCanHasArtistListing extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('members', function(Blueprint $table)
		{
			//wants to be a member or listed artist, but hasn't paid yet
			$table->boolean('desiresMembership')->nullable();
			$table->boolean('desiresListing')->nullable();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('members', function(Blueprint $table)
		{
			$table->dropColumn('desiresMembership');
			$table->dropColumn('desiresListing');
		});
	}

}
