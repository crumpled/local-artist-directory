<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMembersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('members', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('first_name')->nullable();
			$table->string('last_name')->nullable();
			$table->string('email');
			$table->string('phone', 20)->nullable();
			$table->string('mailing')->nullable();
			$table->string('mailing2')->nullable();
			$table->string('mailing_city')->nullable();
			$table->string('mailing_zip', 10)->nullable();
			$table->date('first_membership')->nullable();
			$table->date('membership_expiration')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('members');
	}

}
