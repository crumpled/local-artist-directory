<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCardIdIsPaidArtistIsPaidMemberPaidArtistExpiration extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('members', function(Blueprint $table)
		{
				$table->string('creditcard_id', 40)->nullable();
				$table->date('artist_profile_expiration')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('members', function(Blueprint $table)
		{
			$table->dropColumn('artist_listing');
			$table->dropColumn('artist_profile_expiration');


		});
	}

}
