<?php

class Payment extends \Eloquent {
	protected $fillable = ['member_id', 'payment_id', 'amount', 'state', 'description'];
}
