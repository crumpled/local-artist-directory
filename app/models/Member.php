<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Member extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	protected $hidden = array('password');
	public $errors;

	public static $rules = [
		'first_name' => 'required',
		'last_name' => 'required',
		'email' => 'required|email|unique:members',
		'phone' => 'required',
		'password' => 'required',
	];

	public static $editrules = [

	];

	protected $fillable = [
		'first_name',
		'last_name',
		'email',
		'phone',
		'mailing',
		'mailing2',
		'mailing_city',
		'mailing_state',
		'mailing_zip',
		'password',
		'contact_mailchimp_tcaa',
		'contact_calls_to_artists',
		'contact_class_invites',
		'contact_list_phone',
		'contact_contact_form',
	];

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'members';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	//protected $hidden = array('password', 'remember_token');

	public function isValid(){
		$validator = Validator::make($this->attributes, static::$rules);

		if ($validator->passes()){
			return true;
		}

		$this->errors = $validator->messages();
		return false;
	}

}
