<?php

class Artist extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		'name' => 'required',
		'url_path' => 'unique:artists',
	];

	// Don't forget to fill this array
	protected $fillable = [
	  'name',
	  'business_name',
	  'phone',
	  'photo_url',
	  'desires_arts_reach_inclusion',
	  'gives_lessons',
	  'does_commissions',
	  'web_site',
	  'url_path',
		'artist_bio',
	];

	public function media(){
		return $this->belongsToMany('Medium');
	}

}
