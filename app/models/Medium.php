<?php

class Medium extends \Eloquent {
	// Add your validation rules here
	public static $rules = [
		'name' => 'required|unique:media'
	];
	protected $fillable = ['name'];

	public function artists(){
		return $this->belongsToMany('Artist');
	}
}
