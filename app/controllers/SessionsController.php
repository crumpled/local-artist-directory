<?php

class SessionsController extends \BaseController {

	public function create(){
		return View::make('sessions.create');
	}

	public function store(){
		if (Auth::attempt(Input::only('email', 'password'))){
			// return Auth::user();
			if (Input::has('redirect')) {
				return Redirect::route(Input::get('redirect'));
			} else {
				return Redirect::route('member.edit');
			}

		}
			return Redirect::back()->withInput()->with('loginErrorMessage', 'Email or Password was incorrect.');
	}

	public function destroy(){
		Auth::logout();
		return Redirect::to('/');
	}

}
