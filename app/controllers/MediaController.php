<?php

class MediaController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /media
	 *
	 * @return Response
	 */
	public function index()
	{

	}

	/**
	 * Show the form for creating a new resource.
	 * GET /media/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /media
	 *
	 * @return Response
	 */
	public function store()
	{
		if (Auth::check()) {
			$medium = new Medium();
			$validator = Validator::make($data = Input::all(), Medium::$rules);

			if ($validator->fails())
			{
				// TODO: need more specific errors.
				return Response::make('medium not unique or medium name not specified', 400);
			}

			$medium->fill($data);
			$medium->save();

			return Response::make('', 201);
			// return Redirect::to('member/edit');
		} else {
			return Response::make('You aren\'t logged in', 401);
		}
	}

	/**
	 * Display the specified resource.
	 * GET /media/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$medium = Medium::findOrFail($id);
		$artists = $medium->artists()->get();

		return View::make('media.show', compact('artists', 'medium'));
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /media/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /media/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /media/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
