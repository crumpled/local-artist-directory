<?php

class ArtistsController extends \BaseController {

	public function __construct(Artist $artist) {
		$this->artist = $artist;
		// http://laravel.com/docs/4.2/controllers#controller-filters
		// $this->beforeFilter('auth', array('except' => 'getLogin'));
	}


	/**
	 * Display a listing of artists
	 *
	 * @return Response
	 */
	public function index()
	{
		$artists = Artist::all();

		return View::make('artists.index', compact('artists'));
	}

	/**
	 * Show the form for creating a new artist
	 *
	 * @return Response
	 */
	public function create()
	{
		if (Auth::check())
		{
			$member = Auth::user();
			$query = Artist::where('id', '=', $member->primary_artist_listing );
			$artist = $query->count() ? $query->first() : new Artist();
			$media = Medium::all();
    	return View::make('artists.create', compact('member', 'artist', 'media'));
		}
		else {
			return Redirect::to('/login')->with('redirect', 'artists.create');
		}

	}

  /**
  * Associate the artist listing with a Media type.
	*/
	public function addMedium()
	{
		if (Auth::check()){
			$data = Input::all();
			$member = Auth::user();
			$mediumId = $data['mediumId'];
			$artistId = $data['artistId'];
			$artist = Artist::find($artistId);

			// make sure they are editing themselves only
			// TODO: doesn't support multiple artist listings per member.
			if ($artistId == $member->primary_artist_listing){
				$artist->media()->attach($mediumId);
				return Response::make('', 201);
			}

		}
	}

	/**
	 * Store a newly created artist in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$member = Auth::user();
		$artist = new Artist();
		$validator = Validator::make($data = Input::all(), Artist::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$artist->fill($data);

		$artist->member_id = $member->id;

		// Should this be in the validator? Yeah, probably.
		if (Input::hasFile('profilePic') && $_FILES['profilePic']['size'] > 0 && Input::file('profilePic')->isValid())
		{
			$file = Input::file('profilePic');
			$mimetype = $file->getMimeType();
			$ext = strtolower($file->getClientOriginalExtension());
			$destinationPath = 'memberImages/' . $member->id . '/profiles';
			$filename = str_random(32) . '.' . $ext;

			if (($mimetype == 'image/jpeg' && $ext == ('JPG' || 'JPEG' || 'jpg' || 'jpeg'))||($mimetype == 'image/png' && $ext == ('PNG' || 'png'))||($mimetype == 'image/gif' && $ext == ('GIF' || 'gif'))) {
				$upload_success = $file->move($destinationPath, $filename);
			} else {
				$upload_success = false;
			}

			if( $upload_success ) {
				$artist->photo_url = $destinationPath.'/'.$filename;
				// $artist->photo_url = $mimetype;
				$artist->save();
			}
		} else {
			unset($data->photo_url);
		}

		$artist->save();

		$member->primary_artist_listing = $artist->id;
		$member->save();

		return Redirect::to('member/edit');
	}

	/**
	 * Display the specified artist.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$artist = Artist::findOrFail($id);
		return View::make('artists.show', compact('artist'));
	}

	/**
	 * Show the form for editing the specified artist.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$artist = Artist::find($id);

		return View::make('artists.edit', compact('artist'));
	}

	/**
	 * Update the specified artist in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$artist = Artist::findOrFail($id);
		$member = Auth::user();
		$media = Medium::all();

		$validator = Validator::make($data = Input::all(), Artist::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		// Should this be in the validator? Yeah, probably.
		if (Input::hasFile('profilePic') && $_FILES['profilePic']['size'] > 0 && Input::file('profilePic')->isValid())
		{
			$file = Input::file('profilePic');
			$mimetype = $file->getMimeType();
			$ext = strtolower($file->getClientOriginalExtension());
			$destinationPath = 'memberImages/' . $member->id . '/profiles';
			$filename = str_random(32) . '.' . $ext;

			if (($mimetype == 'image/jpeg' && $ext == ('JPG' || 'JPEG' || 'jpg' || 'jpeg'))||($mimetype == 'image/png' && $ext == ('PNG' || 'png'))||($mimetype == 'image/gif' && $ext == ('GIF' || 'gif'))) {
				$upload_success = $file->move($destinationPath, $filename);
		  } else {
				$upload_success = false;
			}

			if( $upload_success ) {
				$artist->photo_url = $destinationPath.'/'.$filename;
				// $artist->photo_url = $mimetype;
				$artist->save();
			}
		} else {
			unset($data->photo_url);
		}

		$artist->media()->detach();
		$mediaArray = [];
		if ((Input::has('medium3') && Input::get('medium3') != '0') || (Input::has('medium2') && Input::get('medium2') != '0') || (Input::has('medium1') && Input::get('medium1') != '0')){
			if (Input::has('medium3') && Input::get('medium3') != '0') {
				$mediaArray[] = Input::get('medium3');
			}
			if (Input::has('medium2') && Input::get('medium2') != '0') {
				$mediaArray[] = Input::get('medium2');
			}
			if (Input::has('medium1') && Input::get('medium1') != '0') {
				$mediaArray[] = Input::get('medium1');
			}
			$artist->media()->attach($mediaArray);
		} else {
			$artist->publish = 0;
		}
		$artist->update($data);

		return Redirect::route('member.edit');
	}

	/**
	 * Remove the specified artist from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Artist::destroy($id);

		return Redirect::route('artists.index');
	}


	public function publish($id) {
		$artist = Artist::findOrFail($id);
		$artist->publish = $artist->publish == 0 ? 1 : 0;
		$artist->save();
		return Response::make($artist->publish, 200);
	}


}
