<?php
$paypal = require __DIR__ . '/../common/paypal.php';
class MembersController extends \BaseController {

	protected $member;
	protected $artist;

	public function __construct(Member $member, Payment $paymentRecord, Artist $artist) {
		$this->member = $member;
		$this->paymentRecord = $paymentRecord;
		$this->artist = $artist;
		// http://laravel.com/docs/4.2/controllers#controller-filters
		// $this->beforeFilter('auth', array('except' => 'getLogin'));
	}

	/**
	 * Display a listing of members
	 *
	 * @return Response
	 */
	public function index()
	{
		$members = $this->member->all();

		return View::make('members.index', compact('members'));
	}

	/**
	 * Show the form for creating a new member
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('members.create');
	}

	/**
	 * Store a newly created member in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();

		if (!$this->member->fill($input)->isValid()) {
			return Redirect::back()->withInput()->withErrors($this->member->errors);
		}

		$this->member->password = Hash::make(Input::get('password'));
		$this->member->email_confirmation_token = Hash::make(Input::get('email').'jr45989ujit023jhhojjq2g-nh-n0-j40202j4t248=m[2357gpna84vmivem-g-zawq]');


		if (Input::get('membershipLevel') == "Artist"){
			$this->member->desiresListing = true;
		}
		if (Input::get('membershipLevel') == "Member"){
			$this->member->desiresMembership = true;
		}

		$this->member->save();
		Auth::login($this->member);

		// Mail::send('emails.emailConfirmation', array('member_id' => $this->member->id, 'email_confirmation_token' => $this->member->email_confirmation_token, 'first_name' => $this->member->first_name), function($message)
		// {
		// 	$message->to($this->member->email, $this->member->first_name)->subject('Please Confirm Your Email Address');
		// });

		return Redirect::route('members.payment')->with('member', $this->member);
	}

	// Payment Info
	public function payment(){
		if (Auth::check())
		{
			return View::make('members.payment', array('member'=>Auth::user()));
		}
		else {
			return Redirect::to('/login')->with('redirect', 'members.payment');
		}
	}

	// Process Payment
	public function processPayment() {
		if (Auth::check())
		{
			$member = Auth::user();

			// save address
			$member->mailing = Input::get('mailing');
			$member->mailing2 = Input::get('mailing2');
			$member->mailing_city = Input::get('mailing_city');
			$member->mailing_state = Input::get('mailing_state');
			$member->mailing_zip = Input::get('mailing_zip');
			$member->save();

			try {
				$creditCardId = NULL;
							// If CVV2 is not required, we need to remove it. We cannot keep it empty or '' as it is considered your CVV2 is set to ''
							if (isset($_POST['user']['credit_card']['cvv2']) && trim($_POST['user']['credit_card']['cvv2']) == '') {
									unset($_POST['user']['credit_card']['cvv2']);
							}
				// User can configure credit card info later from the
				// profile page or can use paypal as his funding source.
				if(trim($_POST['user']['credit_card']['number']) != "") {
					$creditCardId = saveCard($_POST['user']['credit_card']);
				}

				$member->creditcard_id = $creditCardId;
				$member->save();

				if(!Input::has('updateInfo')){
					//try payment
					$paymentResult = makePaymentUsingCC($creditCardId, Input::get('price'), 'USD', Input::get('description'));

					$paymentRecord = $this->paymentRecord;
					$paymentRecord->member_id = $member->id;
					$paymentRecord->payment_id = $paymentResult->id;
					$paymentRecord->amount = $paymentResult->transactions[0]->amount->total;
					$paymentRecord->state = $paymentResult->state;
					$paymentRecord->save();

					if($paymentResult->state == 'approved'){
							$expiration = new DateTime('today +1 year');
							$expiration->format('Y-m-d');
							if ($member->desiresListing == 1){
								$member->artist_profile_expiration = $expiration;
								$member->desiresListing = NULL;
							}
							if ($member->desiresMembership == 1){
								$member->membership_expiration = $expiration;
								$member->desiresMembership = NULL;
							}
							$member->save();

							return Redirect::route('artists.create');
					} else {
						return 'something went wrong';
					}

				} else {
					return $creditCardId;
				}


			} catch(\PayPal\Exception\PPConnectionException $ex){
				$errorMessage = $ex->getData() != '' ? parseApiError($ex->getData()) : $ex->getMessage();
				return $ex->getData();
			} catch (Exception $ex) {
				$errorMessage = $ex->getMessage();
				// return $errorMessage;
				return $ex->getData();
			}


		}
		else
		{
			return Redirect::to('/login')->with('redirect', 'members.payment');
		}

	}

	public function backFromPayPal($status){
		$member = Auth::user();
		$paymentRecord = $this->paymentRecord;
		$paymentRecord->member_id = $member->id;
		$paymentRecord->payment_id = Input::get('paymentId');
	//	$paymentRecord->amount = $paymentResult->transactions[0]->amount->total;
		$paymentRecord->state = $status;
		$paymentRecord->save();

		if($status == 'success'){
				$expiration = new DateTime('today +1 year');
				$expiration->format('Y-m-d');
				if ($member->desiresListing == 1){
					$member->artist_profile_expiration = $expiration;
					$member->desiresListing = NULL;
				}
				if ($member->desiresMembership == 1){
					$member->membership_expiration = $expiration;
					$member->desiresMembership = NULL;
				}
				$member->save();

				return Redirect::route('member.edit');
		} else {
			return Input::all();
		}

	}

	/**
	 * Display the specified member.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$member = Member::findOrFail($id);

		return View::make('members.show', compact('member'));
	}

	/**
	 * Show the form for editing the specified member.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit() //
	{
		if (Auth::check()){
			$member = Auth::user();
			$query = Artist::where('id', '=', $member->primary_artist_listing );
			$artist = $query->count() ? $query->first() : new Artist();
			$media = Medium::all();
			return View::make('members.edit', compact('member', 'artist', 'media'));
		} else {
			return Redirect::to('/login')->with('redirect', 'member.edit');
		}
	}


	/**
	 * Update the specified member in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
		$member = Auth::user();
		$data = Input::all();

		//$validator = Validator::make($data = Input::all(), Member::$editrules);

		if (!Auth::validate(array('email' => $member->email, 'password' => Input::get('password-check')))){
			return Redirect::route('member.edit')->with(['passwordError'=>'Password was not entered correctly.']);
		}

		// if ($validator->fails())
		// {
		// 	return Redirect::route('member.edit')->withErrors($validator);
		// }

		$member->update($data);
		$member->save();

		return Redirect::route('member.edit');
	}

	/**
	 * Remove the specified member from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//Member::destroy($id);

	  //	return Redirect::route('members.index');
	}

}
