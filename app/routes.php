<?php

Route::any('member/payment', array('as' => 'members.payment', 'uses' => 'MembersController@payment'));
Route::post('artist/medium', array('as' => 'artists.medium', 'uses' => 'ArtistsController@addMedium'));
Route::post('artists/{id}/publish', array('as' => 'artists.publish', 'uses' => 'ArtistsController@publish'));
Route::post('member/processPayment', array('as' => 'members.processPayment', 'uses' => 'MembersController@processPayment'));
Route::get('member/backFromPaypal/{status}', array('as' => 'members.backFromPaypal', 'uses' => 'MembersController@backFromPaypal'));
Route::get('member/edit', array('as' => 'member.edit', 'uses' => 'MembersController@edit'));
Route::any('member/update', array('as' => 'member.update', 'uses' => 'MembersController@update'));
Route::any('password/remind', array('as' => 'password.remind', 'uses' => 'RemindersController@postRemind'));
Route::get('password/forgot', array('uses' => 'RemindersController@getRemind'));
Route::get('password/reset/{token}', array('as' => 'password.reset', 'uses' => 'RemindersController@getReset'));
Route::post('password/reset', array('as' => 'password.postReset', 'uses' => 'RemindersController@postReset'));


Route::resource('members', 'MembersController');
Route::resource('artists', 'ArtistsController');
Route::resource('sessions', 'SessionsController');
Route::resource('payments', 'PaymentsController');
Route::resource('reminders', 'RemindersController');
Route::resource('media', 'MediaController');

Route::get('/', function()
{
	$artists = Artist::where('publish','=','1')->get()->sortByDesc('updated_at');
	return View::make('hello')->with('artists', $artists);
});

# following laracast examples, we already get default routes using the Resourcee
Route::get('login', 'SessionsController@create');
Route::get('logout', 'SessionsController@destroy');
