  <div class="well">
    @if($artist->photo_url != null)
      <a href="/{{{$artist->photo_url}}}">
        <img src="/{{{$artist->photo_url}}}" alt="{{{$artist->business_name}}}" class="img-responsive full-profile-image">
      </a>
    @else
      <p><span class="label label-default">No Profile Image</span></p>
    @endif
    <h3 class="artist-biz-name">{{{$artist->business_name}}}</h3>
    <h4 class="artist-name">{{{$artist->name}}}</h4>
    <p class="artist-contact">
      <span class="artist-web-site"><a href="{{{$artist->web_site}}}">{{{$artist->web_site}}}</a></span><br>
      <span class="artist-phone">{{{$artist->phone}}}</span><br>
    </p>
    <p class="artist-artist-bio">{{{$artist->artist_bio}}}</p>
    @if(!$artist->media()->get()->isEmpty())
      <div class="artist-media">
        <h5>Media</h5>
        @foreach($artist->media()->get() as $medium)
          <div class="artist-medium"><a href="/media/{{$medium->id}}">{{$medium->name}}</a></div>
        @endforeach
      </div>
    @endif
  </div>
