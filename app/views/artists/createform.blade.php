<div class="row">
  <div class="col-xs-12">
    @if(!$member->primary_artist_listing)
    <?php $editing = false; ?>
    <h2>New Artist Listing</h2>
    {{ Form::open(['route' => 'artists.store']) }}
    @else
    <?php $editing = true; ?>
    {{ Form::model($artist, array('route' => array('artists.update', $artist->id), 'method' => 'put', 'files' => true)) }}
    @endif

      <!-- <div class="input-group">
        <span class="input-group-addon" id="labelUrlPath">https://{{$_SERVER['SERVER_NAME']}}/artist/</span>
        {{ Form::text('url_path', '', array('class' => 'form-control', 'aria-describedby' => 'labelUrlPath')) }}
      </div>
      @if ($errors->has('url_path'))
        @foreach ($errors->get('url_path') as $message)
        <div class="alert alert-danger" role="alert">
          {{ $message }}
        </div>
        @endforeach
      @endif -->

      @if($editing == true)
        <div class="input-group">
          <span class="input-group-addon" id="labelBioPic">Change Profile Picture</span>
          {{ Form::file('profilePic', array('class' => 'form-control', 'aria-describedby' => 'labelBioPic')) }}
        </div>
        @if ($errors->has('profilePic'))
          @foreach ($errors->get('profilePic') as $message)
          <div class="alert alert-danger" role="alert">
            {{ $message }}
          </div>
          @endforeach
        @endif
      @endif

      <div class="input-group">
        <span class="input-group-addon" id="labelName">Artist Name</span>
        {{ Form::text('name', $artist->name, array('class' => 'form-control', 'aria-describedby' => 'labelName')) }}
      </div>
      @if ($errors->has('name'))
        @foreach ($errors->get('name') as $message)
        <div class="alert alert-danger" role="alert">
          {{ $message }}
        </div>
        @endforeach
      @endif

      <div class="input-group">
        <span class="input-group-addon" id="labelBusinessName">Alternate/Business Name</span>
        {{ Form::text('business_name', $artist->business_name, array('class' => 'form-control', 'aria-describedby' => 'labelBusinessName')) }}
      </div>
      @if ($errors->has('business_name'))
        @foreach ($errors->get('business_name') as $message)
        <div class="alert alert-danger" role="alert">
          {{ $message }}
        </div>
        @endforeach
      @endif

      <div class="input-group">
        <span class="input-group-addon" id="labelPhone">Phone Number</span>
        {{ Form::text('phone', $artist->phone, array('class' => 'form-control', 'aria-describedby' => 'labelPhone')) }}
      </div>
      @if ($errors->has('phone'))
        @foreach ($errors->get('phone') as $message)
        <div class="alert alert-danger" role="alert">
          {{ $message }}
        </div>
        @endforeach
      @endif

<!--
      'desires_arts_reach_inclusion',

      'gives_lessons',

      'does_commissions', -->

      <div class="input-group">
        <span class="input-group-addon" id="labelWebSite">Web Site</span>
        {{ Form::text('web_site', $artist->web_site, array('class' => 'form-control', 'aria-describedby' => 'labelWebSite')) }}
      </div>
      @if ($errors->has('web_site'))
        @foreach ($errors->get('web_site') as $message)
        <div class="alert alert-danger" role="alert">
          {{ $message }}
        </div>
        @endforeach
      @endif

      <div class="input-group">
        <span class="input-group-addon" id="labelArtistBio">Artist Bio</span>
        {{ Form::textarea('artist_bio', $artist->artist_bio, array('class' => 'form-control', 'aria-describedby' => 'labelArtistBio')) }}
      </div>
      @if ($errors->has('artist_bio'))
        @foreach ($errors->get('artist_bio') as $message)
        <div class="alert alert-danger" role="alert">
          {{ $message }}
        </div>
        @endforeach
      @endif

      @if($editing == true)
        <div class="input-group">
          <span class="input-group-addon" id="labelCategories">Artist Medium <br>(Choose up to three Media)</span>
          <?php $maxFields = 3;
          $topmedia = Medium::whereNull('parent')->get();?>
          @foreach ($artist->media()->get() as $artistMedium)
            <select name="medium{{$maxFields}}" class="form-control">
              <option value="0">None</option>
              <option disabled>──</option>
              @foreach ($topmedia as $medium)
                  <option value="{{{$medium->id}}}"><strong>{{{strtoupper($medium->name)}}}</strong></option>
                  <?php $submedia = Medium::where('parent','=', $medium->id)->get(); ?>
                  @foreach($submedia as $submedium)
                    <option value="{{{$submedium->id}}}" <?php
                    echo ($submedium->id == $artistMedium->id) ? "selected" : "";
                    ?>>- {{{strtolower($submedium->name)}}}</option>
                  @endforeach
                  <option disabled>──</option>
              @endforeach
            </select>
            <?php $maxFields--; ?>
          @endforeach
          @while($maxFields > 0)
          <select name="medium{{$maxFields}}" class="form-control">
            <option value="0">None</option>
            <option disabled>──</option>
            @foreach ($topmedia as $medium)
                <option value="{{{$medium->id}}}"><strong>{{{strtoupper($medium->name)}}}</strong></option>
                <?php $submedia = Medium::where('parent','=', $medium->id)->get()->sortBy('name'); ?>
                @foreach($submedia as $submedium)
                    <option value="{{{$submedium->id}}}">- {{{strtolower($submedium->name)}}}</option>
                @endforeach
                <option disabled>──</option>
            @endforeach
          </select>
          <?php $maxFields--; ?>
          @endwhile
        </div>
      @endif

      @if($editing == false)
      <button type="submit" class="btn btn-success">Add Listing</button>
      @else
      <button type="submit" class="btn btn-success">Edit Listing</button>
      @endif
      {{ Form::close()}}

    </div>

  </div>
