  <div class="well">
    <div class="preview-profile-url">
      @if($artist->photo_url != null)
        <img src="/{{{$artist->photo_url}}}" alt="{{{$artist->business_name}}}" class="small-profile-image img-responsive img-circle">
      @else
        <p><span class="label label-default">No Profile Image</span></p>
      @endif
    </div>
    <h3 class="artist-biz-name">{{{$artist->business_name}}}</h3>
    <h4 class="artist-name">{{{$artist->name}}}</h4>
    <p class="artist-contact">
      <span class="artist-web-site"><a href="{{{$artist->web_site}}}" target="_blank">{{{$artist->web_site}}}</a></span>
      <span class="artist-phone">{{{$artist->phone}}}</span>
    </p>
    <p class="artist-artist-bio">{{{$artist->artist_bio}}}</p>
    @if(!$artist->media()->get()->isEmpty())
      <div class="artist-media">
        <h5>Media</h5>
        @foreach($artist->media()->get() as $medium)
          <div class="artist-medium"><a href="/media/{{$medium->id}}" target="_top">{{$medium->name}}</a></div>
        @endforeach
      </div>
    @endif
    <hr>
    <p><span class="link-to-artist"><a href="/artists/{{$artist->id}}" target="_top">More...</a></span></p>
  </div>
