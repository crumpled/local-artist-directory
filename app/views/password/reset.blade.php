@extends('layouts.default')
@section('body_content')
<div class="jumbotron">
  <h2>Reset Your Password</h2>
  <p>Choose a new password below.</p>
  <form action="{{ action('RemindersController@postReset') }}" method="POST">
      <input type="hidden" name="token" value="{{ $token }}">
      <div class="input-group">
        <span class="input-group-addon" id="labelEmail">User Email Address</span>
        {{ Form::email('email', $member->email, array('class' => 'form-control', 'aria-describedby' => 'labelEmail')) }}
      </div>
      @if (gettype($errors) == 'string')
        <div class="alert alert-danger" role="alert">
          {{ $errors }}
        </div>
      @endif
      <div class="input-group">
        <span class="input-group-addon" id="labelPassword">Password</span>
        <input type="password" class="form-control" name="password" aria-describedby="labelPassword">
      </div>
      <div class="input-group">
        <span class="input-group-addon" id="labelConfirm">Confirm Password</span>
        <input type="password" class="form-control" name="password_confirmation" aria-describedby="labelConfirm">
      </div>
      <br>
      <button type="submit" class="btn btn-success">Reset Password</button>
  </form>
</div>
@stop
