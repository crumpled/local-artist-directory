@extends('layouts.default')

@section('body_content')
<div class="jumbotron">
  <h2>Reset Your Password</h2>
  <p>Use this form to request a password reset link, sent to your email address.</p>
  {{ Form::open(['route' => 'password.remind']) }}
      <div class="input-group">
        <span class="input-group-addon" id="labelEmail">User Email Address</span>
        {{ Form::email('email', '', array('class' => 'form-control', 'aria-describedby' => 'labelEmail')) }}
      </div>
      @if (gettype($errors) == 'string')
        <div class="alert alert-danger" role="alert">
          {{ $errors }}
        </div>
      @endif
      <br>
      <button type="submit" class="btn btn-success">Request New Password</button>
  {{ Form::close() }}
</div>
@stop
