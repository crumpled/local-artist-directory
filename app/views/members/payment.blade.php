@extends('layouts.default')
@section('body_content')
	<div class="jumbotron">
		<h1>Payment Info</h1>
		<p>
			{{ Form::open(['route' => 'members.processPayment']) }}

			<div class="input-group">
				<span class="input-group-addon" id="labelMailingOne">Billing Address Line 1</span>
				{{ Form::text('mailing', '', array('class' => 'form-control', 'aria-describedby' => 'labelMailingOne')) }}
			</div>

			<div class="input-group">
				<span class="input-group-addon" id="labelMailingTwo">Billing Address Line 2</span>
				{{ Form::text('mailing2', '', array('class' => 'form-control', 'aria-describedby' => 'labelMailingTwo')) }}
			</div>

			<div class="input-group">
				<span class="input-group-addon" id="labelCity">City</span>
				{{ Form::text('mailing_city', '', array('class' => 'form-control', 'aria-describedby' => 'labelCity')) }}
			</div>

			<div class="input-group">
				<span class="input-group-addon" id="labelState">State</span>
				{{ Form::text('mailing_state', 'CA', array('class' => 'form-control', 'aria-describedby' => 'labelState')) }}
			</div>

			<div class="input-group">
				<span class="input-group-addon" id="labelZip">Zip Code</span>
				{{ Form::text('mailing_zip', '', array('class' => 'form-control', 'aria-describedby' => 'labelPhone')) }}
			</div>


			<div class="well">
				<h2>Credit Card</h2>
				<div class="input-group">
					<span class="input-group-addon" id="labelFirstName">First Name</span>
					{{ Form::text('user[credit_card][first_name]', $member->first_name, array('class' => 'form-control', 'aria-describedby' => 'labelFirstName', 'id' => 'user_credit_card_first_name')) }}
				</div>

				<div class="input-group">
					<span class="input-group-addon" id="labelCardNumber">Last Name</span>
					{{ Form::text('user[credit_card][last_name]', $member->last_name , array('class' => 'form-control', 'aria-describedby' => 'labelCardNumber', 'id' => 'user_credit_card_last_name')) }}
				</div>

				<div class="input-group">
					<span class="input-group-addon" id="labelCardType">Card Type</span>
					{{Form::select('user[credit_card][type]', array('visa' => 'Visa', 'mastercard' => 'Mastercard', 'discover' => 'Discover', 'amex' => 'American Express'), '', array('class' => 'form-control select required', 'aria-describedby' => 'labelCardType', 'id'=> 'user_credit_card_type'))}}
				</div>

				<div class="input-group">
					<span class="input-group-addon" id="labelCardNumber">Card Number</span>
					{{ Form::text('user[credit_card][number]', '', array('class' => 'form-control', 'aria-describedby' => 'labelCardNumber', 'id' => 'user_credit_card_number')) }}
				</div>

				<div class="input-group">
					<span class="input-group-addon" id="labelCVV">3 Digit Security Code</span>
					{{ Form::text('user[credit_card][cvv2]', '', array('class' => 'form-control', 'aria-describedby' => 'labelCVV', 'id' => 'user_credit_card_cvv2')) }}
				</div>

				<div class="input-group">
					<span class="input-group-addon" id="labelExpMonth">Expiration Month</span>
					{{Form::select('user[credit_card][expire_month]', array('1' => '1 Jan', '2' => '2 Feb', '3' => '3 Mar', '4' => '4 Apr', '5' => '5 May', '6' => '6 Jun', '7' => '7 Jul', '8' => '8 Aug', '9' => '9 Sep', '10' => '10 Oct','11' => '11 Nov', '12' => '12 Dec'), '', array('class' => 'form-control select required', 'aria-describedby' => 'labelExpMonth', 'id'=> 'user_credit_card_expire_month'))}}
				</div>

				<div class="input-group">
					<span class="input-group-addon" id="labelExpYear">Expiration Year</span>
					{{Form::select('user[credit_card][expire_year]', array('2015' => '2015', '2016' => '2016', '2017' => '2017', '2018' => '2018', '2019' => '2019', '2020' => '2020', '2021' => '2021', '2022' => '2022', '2023' => '2023', '2024' => '2024','2025' => '2025'), '', array('class' => 'form-control select required', 'aria-describedby' => 'labelExpYear', 'id'=> 'user_credit_card_expire_year'))}}
				</div>

			</div>

			@if($member->desiresListing)
				<p>Payment total will be $10</p>
				{{ Form::hidden('price', '10.00') }}
				{{ Form::hidden('description', 'Artist Directory Listing: One Year') }}
				<button type="submit" class="btn btn-success">Checkout</button>
			@elseif($member->desiresMembership)
				<p>Payment total will be $45</p>
				{{ Form::hidden('price', '45.00') }}
				{{ Form::hidden('description', 'TCAA Membership: One Year') }}
				<button type="submit" class="btn btn-success">Process Payment</button>
			@else
				{{ Form::hidden('updateInfo','1')}}
				<button type="submit" class="btn btn-default" disabled>Update</button>
			@endif

			{{ Form::close() }}
		</p>
	</div>
@endsection

@section('scripts')
	<script>
	(function(){
		var radios = $( "input[type='radio'][name='paymentMethod']" );
		radios.change(function(e) {
				var val = $(this).val();
				if(val == 'PayPal'){
					$('#ccInfo').collapse('hide');
				}
				if(val == 'CC'){
					$('#ccInfo').collapse('show');
				}
				console.log(e, $(this));
				//debugger;
		});
		radios.filter('[checked]').trigger('change');
	}());
	</script>
@endsection
