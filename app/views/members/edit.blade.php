@extends('layouts.default')
@section('body_content')
<div class="row">
  <div class="col-xs-12 col-sm-9">

    <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseProfile" aria-expanded="false" aria-controls="collapseProfile">Edit Personal Info</button>
    <div class="collapse" id="collapseProfile">
      <div class="well">
        {{ Form::open(['url' => 'member/update']) }}
        <div class="input-group">
          <span class="input-group-addon" id="labelFirstName">First Name</span>
          {{ Form::text('first_name', $member->first_name, array('class' => 'form-control', 'aria-describedby' => 'labelFirstName')) }}
        </div>
        @if ($errors->has('first_name'))
          @foreach ($errors->get('first_name') as $message)
          <div class="alert alert-danger" role="alert">
            {{ $message }}
          </div>
          @endforeach
        @endif

        <div class="input-group">
          <span class="input-group-addon" id="labelLastName">Last Name</span>
          {{ Form::text('last_name', $member->last_name, array('class' => 'form-control', 'aria-describedby' => 'labelLastName')) }}
        </div>
        @if ($errors->has('last_name'))
          @foreach ($errors->get('last_name') as $message)
          <div class="alert alert-danger" role="alert">
            {{ $message }}
          </div>
          @endforeach
        @endif

        <div class="input-group">
          <span class="input-group-addon" id="labelEmail">Email</span>
          {{ Form::email('email', $member->email, array('class' => 'form-control', 'aria-describedby' => 'labelEmail', 'disabled' => true, 'value' => $member->email)) }}
        </div>
        @if ($errors->has('email'))
          @foreach ($errors->get('email') as $message)
          <div class="alert alert-danger" role="alert">
            {{ $message }}
          </div>
          @endforeach
        @endif

        <div class="input-group">
          <span class="input-group-addon" id="labelPhone">Phone Number</span>
          {{ Form::text('phone', $member->phone, array('class' => 'form-control', 'aria-describedby' => 'labelPhone')) }}
        </div>
        @if ($errors->has('phone'))
          @foreach ($errors->get('phone') as $message)
          <div class="alert alert-danger" role="alert">
            {{ $message }}
          </div>
          @endforeach
        @endif

        <p>Re-enter your password to save changes.</p>
        <div class="input-group">
          <span class="input-group-addon" id="labelPassword">Password</span>
          <input type="password" class="form-control" name="password-check" aria-describedby="labelPassword">
        </div>
        @if (null !== Session::get('passwordError'))
          <div class="alert alert-danger" role="alert">
            {{ Session::get('passwordError') }}
          </div>
        @endif

        <button type="submit" class="btn btn-success">Save</button>
        {{ Form::close() }}
      </div>
    </div>
    <div class="clear"></div>

    <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseContact" aria-expanded="false" aria-controls="collapseContact">Edit Contact Preferences</button>
    <div class="collapse" id="collapseContact">
      <div class="well">
        {{ Form::open(['url' => 'member/update']) }}
        <div class="input-group">
          <span class="input-group-addon" id="labelContact_mailchimp_tcaa">Receive TCAA News</span>
          <span class="input-group-addon for-input">
            <input aria-describedby="labelContact_mailchimp_tcaa" data-realField="contact_mailchimp_tcaa" @if($member->contact_mailchimp_tcaa) checked @endif name="_contact_mailchimp_tcaa" type="checkbox">
            {{ Form::hidden('contact_mailchimp_tcaa', $member->contact_mailchimp_tcaa) }}
          </span>
        </div>

        <div class="input-group">
          <span class="input-group-addon" id="labelContact_calls_to_artists">Receive Call-To-Artist Opportunities</span>
          <span class="input-group-addon for-input">
            <input aria-describedby="labelContact_calls_to_artists" data-realField="contact_calls_to_artists" @if($member->contact_calls_to_artists) checked @endif name="_contact_calls_to_artists" type="checkbox">
            {{ Form::hidden('contact_calls_to_artists', $member->contact_calls_to_artists) }}
          </span>
        </div>

        <div class="input-group">
          <span class="input-group-addon" id="labelContact_class_invites">Receive Class/Workshop Invitations</span>
          <span class="input-group-addon for-input">
            <input aria-describedby="labelContact_class_invites" data-realField="contact_class_invites" @if($member->contact_class_invites) checked @endif name="_contact_class_invites" type="checkbox">
            {{ Form::hidden('contact_class_invites', $member->contact_class_invites) }}
          </span>
        </div>

        <div class="input-group">
          <span class="input-group-addon" id="labelContact_list_phone">Publish Phone Number in Artist Listing</span>
          <span class="input-group-addon for-input">
            <input aria-describedby="labelContact_list_phone" data-realField="contact_list_phone" @if($member->contact_list_phone) checked @endif name="_contact_list_phone" type="checkbox">
            {{ Form::hidden('contact_list_phone', $member->contact_list_phone) }}
          </span>
        </div>

        <div class="input-group">
          <span class="input-group-addon" id="labelContact_contact_form">Receive email from visitors. <br>(we won't show your email address)</span>
          <span class="input-group-addon for-input">
            <input aria-describedby="labelContact_contact_form" data-realField="contact_contact_form" @if($member->contact_contact_form) checked @endif name="_contact_contact_form" type="checkbox">
            {{ Form::hidden('contact_contact_form', $member->contact_contact_form) }}
          </span>
        </div>

        <p>Re-enter your password to save changes.</p>
        <div class="input-group">
          <span class="input-group-addon" id="labelPassword">Password</span>
          <input type="password" class="form-control" name="password-check" aria-describedby="labelPassword">
        </div>
        @if (null !== Session::get('passwordError'))
          <div class="alert alert-danger" role="alert">
            {{ Session::get('passwordError') }}
          </div>
        @endif

        <button type="submit" class="btn btn-success">Save</button>
        {{ Form::close() }}
      </div>
    </div>
    <div class="clear"></div>

    @if($member->desiresListing || $member->desiresMembership)
      <div class="alert alert-warning" role="alert">
        <p>
          Payment is required before your artist listing will be published.
        </p>
        <a class="btn btn-success" href="/member/payment">Go to Payment page</a>
      </div>
    @endif

    @if($artist->id)
    <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseArtist" aria-expanded="false" aria-controls="collapseArtist">Edit Artist Profile</button>@if(!$artist->media()->exists()) <span class="label label-warning">Add a Medium to publish your Artist Profile</span>@endif
    <div class="collapse" id="collapseArtist">
      <div class="well">

        @include('artists.createform')

      </div>
    </div>
    <div class="clear"></div>
    @else
      <a href="/artists/create" class="btn btn-success">Create Artist Listing</a>
    @endif
    @if($artist->media()->exists())
    <div class="onoffswitch">
      <input type="checkbox" name="publishswitch{{$artist->id}}" class="onoffswitch-checkbox" data-artist-id="{{$artist->id}}" id="publishswitch{{$artist->id}}" {{ $artist->publish == 1 ? 'checked' : '' }} >
      <label class="onoffswitch-label" for="publishswitch{{$artist->id}}">Publish</label>
    </div>
    @endif

  </div>
  <div class="col-xs-12 col-sm-3">
    <div class="well">
      <?php /* membership should be calculated by a method on the Member class, not like this... */ ?>
      @if($member->membership_expiration)
        <img src="/img/tcaa-member-logo.png" alt="TCAA Member" class="img-responsive">
        <p>Member ID: {{ $member->id }}</p>
        <?php
          $exp = strtotime($member->membership_expiration);
          $expstr = new DateTime($member->membership_expiration);
         $expstr = $expstr->format('m/d/Y');
         ?>
        @if($exp >= time())
          <p>Expires: {{ $expstr }}</p>
          <p>Thank you for supporting the arts.</p>
        @else
          <p class="alert alert-warning">Expired: {{ $expstr }}</p>
        @endif
      @else
        <h4>Support the Arts.</h4>
        <p>Consider becoming a supporting member of the Tuolumne County Arts Alliance.</p>
      @endif
    </div>
  </div>
</div>
<div class="row">
  <div class="col-xs-12 col-sm-9">
    <h3>Full Profile</h3>
    @include('artists.fullListing')
  </div>
  <div class="col-xs-6 col-sm-3">
    <h3>Listing Preview</h3>
    @include('artists.preview')
  </div>
</div>
@endsection
@section('scripts')
  <script>
    (function(){
      $('input.form-control[type="checkbox"]').on('click', function(e){
        var el = $(this),
          realField = $('[name="'+el.attr('data-realField')+'"]');
        if (el.attr('checked')){
          realField.val(0);
          el.removeAttr('checked');
        } else {
          realField.val(1);
          el.attr({'checked':'checked'});
        }
        e.stopImmediatePropagation();
      });

      $('.onoffswitch-checkbox').on('click', function(e){
        var el = $(this),
          artistID = el.attr('data-artist-id');
        $.ajax('/artists/'+artistID+'/publish', { method: 'POST' }).done(function(data){
          if (data == 0){
            el.removeAttr('checked')
          }
          if (data == 1){
            el.attr('checked', true);
          }
        });
      });


    }())
  </script>
@endsection
