@extends('layouts.default')
@section('body_content')
	@if($members->count())
		<h1>All Members</h1>
		<ul>
			@foreach ($members as $member)
				<li>{{ link_to("/members/{$member->id}", $member->first_name.' '.$member->last_name) }} </li>
			@endforeach
		</ul>
	@else
		no results
	@endif
@stop
