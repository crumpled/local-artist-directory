@extends('layouts.default')
@section('body_content')
	<div class="jumbotron">
		<h1>New Member</h1>
		<p>
			{{ Form::open(['route' => 'members.store']) }}
			<p>Fill out this form to join. All fields are required.</p>
			<div class="input-group">
				<span class="input-group-addon" id="labelFirstName">First Name</span>
				{{ Form::text('first_name', '', array('class' => 'form-control', 'aria-describedby' => 'labelFirstName')) }}
			</div>
			@if ($errors->has('first_name'))
				@foreach ($errors->get('first_name') as $message)
				<div class="alert alert-danger" role="alert">
					{{ $message }}
				</div>
				@endforeach
			@endif

			<div class="input-group">
				<span class="input-group-addon" id="labelLastName">Last Name</span>
				{{ Form::text('last_name', '', array('class' => 'form-control', 'aria-describedby' => 'labelLastName')) }}
			</div>
			@if ($errors->has('last_name'))
				@foreach ($errors->get('last_name') as $message)
				<div class="alert alert-danger" role="alert">
					{{ $message }}
				</div>
				@endforeach
			@endif

			<div class="input-group">
				<span class="input-group-addon" id="labelEmail">Email</span>
				{{ Form::email('email', '', array('class' => 'form-control', 'aria-describedby' => 'labelEmail')) }}
			</div>
			@if ($errors->has('email'))
				@foreach ($errors->get('email') as $message)
				<div class="alert alert-danger" role="alert">
					{{ $message }}
				</div>
				@endforeach
			@endif

			<div class="input-group">
				<span class="input-group-addon" id="labelPhone">Phone Number</span>
				{{ Form::text('phone', '', array('class' => 'form-control', 'aria-describedby' => 'labelPhone')) }}
			</div>
			@if ($errors->has('phone'))
				@foreach ($errors->get('phone') as $message)
				<div class="alert alert-danger" role="alert">
					{{ $message }}
				</div>
				@endforeach
			@endif

			<div class="input-group">
				<span class="input-group-addon" id="labelPassword">Password</span>
				<input type="password" class="form-control" name="password" aria-describedby="labelPassword">
			</div>
			@if ($errors->has('password'))
				@foreach ($errors->get('password') as $message)
				<div class="alert alert-danger" role="alert">
					{{ $message }}
				</div>
				@endforeach
			@endif

			<p>Sign up now for a full TCAA membership, and get your Artist Directory Listing included.</p>
			<p>Your TCAA membership helps us cover operating expenses, and allows us to bring Arts Education to our community.</p>

			<div class="row">
				<div class="col-xs-5">
					<div class="input-group">
			      <span class="form-control control-description">Directory Only [$10]</span>
						<span class="input-group-addon for-input">
							<input type="radio" name="membershipLevel" value="Artist" checked>
						</span>
		    	</div><!-- /input-group -->
				</div>
				<div class="col-xs-2"><p>or</p></div>
				<div class="col-xs-5">
					<div class="input-group">
			      <span class="form-control control-description">Full Membership [$45]</span>
						<span class="input-group-addon for-input">
			        <input type="radio" name="membershipLevel" value="Member">
			      </span>
		    	</div><!-- /input-group -->
				</div>
			</div>

			<button type="submit" class="btn btn-success">Join</button>
		{{ Form::close() }}
	</p>
	</div>
@endsection
