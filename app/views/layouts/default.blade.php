<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>TCAA Arts Directory</title>
	<link rel="stylesheet" href="/css/bootstrap.min.css">
	<link rel="stylesheet" href="/css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="/css/tcaa-styles.css">

	<?php /* font-family: "franklin-gothic-urw";
	 (domain might be localhost or *.tuolumnecountyarts.org) */ ?>
	<script src="https://use.typekit.net/was1nsl.js"></script>
	<script>try{Typekit.load({ async: true });}catch(e){}</script>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	  <!--[if lt IE 9]>
	    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	  <![endif]-->
</head>
<body>
	<div class="container">
		<div class="header clearfix">
			<nav>
				<ul class="nav nav-pills pull-right">
					@yield('nav_ul')
					@if(Auth::check())
						<li role="presentation" class="top-only"><a href="/member/edit" target="_top">Edit {{ Auth::user()->first_name }}</a></li>
						<li role="presentation" class="top-only"><a href="/logout" target="_top">Log Out</a></li>
					@else
						<li role="presentation" class="top-only"><a href="/login"  target="_top">Log In</a></li>
					@endif
				</ul>
			</nav>
			<div class="logotype"><a href="/" target="_top">
				<span class="ad-artist">Arts</span><span class="ad-directory">Directory</span>
			</a></div>
		</div>
		@yield('body_content')
	</div>

	<footer class="footer">
      <div class="container">
        <p class="text-muted"> TCAA Artist Directory (Public Alpha) -- <a target="_blank" href="https://bitbucket.org/crumpled/local-artist-directory/issues/new">Report Issues or Request Features</a> @yield('footer')</p>
      </div>
    </footer>
	<script src="/js/jquery-1.11.3.min.js"></script>
	<script src="/js/bootstrap.min.js"></script>
	<script>
	(
		//disable submit buttons on click
		function(){
		$('button[type="submit"]').on('click', function(){
			$(this).attr('disabled', true);
			$(this).closest('form').submit()
		})
	}())
	</script>

	@yield('scripts')
</body>
</html>
