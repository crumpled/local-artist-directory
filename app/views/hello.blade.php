@extends('layouts.default')

@section('nav_ul')
	<li role="presentation" class="active"><a href="/member/edit" target="_top">Create Your Listing</a></li>

@stop
@section('body_content')
<!-- <div class="row">
	<div class="col-xs-12">
		<div class="logotype"><a href="/">
			<span class="ad-artist">Arts</span><span class="ad-directory">Directory</span>
		</a></div>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<h4>Recent Profiles</h4>
	</div>
</div> -->
<div class="row">
@foreach($artists as $artist)
	<div class="col-xs-6 col-sm-3">
		@include('artists.preview')
	</div>
@endforeach
</div>
@stop

@section('scripts')
<script>
	// hide header and footer when displayed in an iframe
	(function(){
		if(self != top){
			// $('.header, .footer').hide();
			$('.top-only').hide();
			$('body').css('padding-top', '0');
			$('.container').css({'padding-right':'0', 'padding-left':'0'});
		}
	}())
</script>
@stop
