@extends('layouts.default')
@section('body_content')
	<div class="jumbotron">
		<p>Log in or sign up to continue.</p>
		<p>
			{{ Form::open(['route' => 'sessions.store']) }}
				@if(null !== Session::get('loginErrorMessage'))
					<div class="alert alert-danger" role="alert">{{ Session::get('loginErrorMessage') }}</div>
				@endif
				<div class="input-group">
					<span class="input-group-addon" id="labelEmail">Email</span>
					{{ Form::email('email', '', array('class' => 'form-control', 'aria-describedby' => 'labelEmail')) }}
				</div>
				<div class="input-group">
					<span class="input-group-addon" id="labelPassword">Password</span>
					<input type="password" class="form-control" name="password" aria-describedby="labelPassword">
				</div>
				<p><a href="/password/forgot">Password Not Working? Click here.</a></p>
				{{ Form::hidden('redirect', Session::get('redirect')) }}
				<!-- <span class="forget-pw-link"><a href="#">Forgot Password?</a></span><br> -->
				<button type="submit" class="btn btn-default">Log In</button>
			{{ Form::close() }}
			<hr>
			<h3>Don't have an account?</h3>
			<span class="sign-up-link"><a href="/members/create"class="btn btn-success">Sign Up</a></span>
		</p>

	</div>

@endsection
