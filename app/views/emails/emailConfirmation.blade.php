<p>Hello {{ $name }},</p>

<p>Welcome, and thank you for supporting Tuolumne County Arts Alliance.</p>

<p>Member ID: {{ $member_id }} <br>
Email Confirmation Code: {{ $email_confirmation_token }}</p>

<p>This step is required in order to publish your directory listing.</p>

<p>Thanks! <br>
-The TCAA Artist Directory Robot</p>
