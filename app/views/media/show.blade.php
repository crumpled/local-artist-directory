@extends('layouts.default')
@section('body_content')
<h2>
  @if($medium->parent != NULL)
  <?php $parent = Medium::find($medium->parent) ?>
  {{$parent->name}} > 
  @endif
  {{{$medium->name}}}
</h2>
<div class="row">
  <div class="col-xs-6 col-sm-9">
    @foreach($artists as $artist)
      <div class="col-xs-12 col-sm-4 masonic">
      @include('artists.preview', ['artist'=>$artist] )
    </div>
    @endforeach
  </div>
  <?php $submedia = Medium::where('parent', '=', $medium->id)->get() ?>
  @if(!$submedia->isEmpty())
  <div class="col-sm-3">
    <h3>Browse sub-categories</h3>
    <ul>
        @foreach($submedia as $submedium)
          <li><a href="/media/{{$submedium->id}}">{{$submedium->name}}</a></li>
        @endforeach
    </ul>
  </div>
  @endif
</div>
@endsection
