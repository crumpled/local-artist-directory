## TCAA Artists Directory

This project is based off of Laravel 4. It requires the following additional Laravel files to work:
* app/config/app.php
* app/config/database.php
* app/config/mail.php

It also requires a app/common/paypal.php file not currently provided in this repo. In the future, this repo will contain default versions of these files.
